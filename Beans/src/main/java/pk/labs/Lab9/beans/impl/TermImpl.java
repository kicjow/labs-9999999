/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

/**
 *
 * @author Bartek
 */import java.util.Calendar;
import java.util.Date;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import pk.labs.Lab9.beans.Term;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

public class TermImpl extends java.lang.Object implements Serializable, Term 
{

    public PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public ConsultationList lsita;
    public Consultation konsultacja;
    private Date dat = new Date();
    private int duration;
    Calendar kalendarz;
    
    public Date getBegin() 
    {
        return dat;
    }

    public void setBegin(Date begin) 
    {
        this.dat = begin;
    }
    
      @Override
    public int getDuration() 
    {
        return this.duration;
    }

    @Override
    public void setDuration(int duration) 
    {
        if (duration < 0) 
        {
            return;
        }
        if (duration > 0) 
        {
            this.duration = duration;
        }
    }
    
    public TermImpl(Date dat, int duration)
    {
        this.dat = dat;
        this.duration = duration;
        pcs = new PropertyChangeSupport(this);      
    }
    
    public TermImpl() 
    {
        dat = new Date();
        pcs = new PropertyChangeSupport(this);    
    }

      @Override
    public Date getEnd() 
    {
        kalendarz = Calendar.getInstance();
        kalendarz.setTime(dat);
        kalendarz.add(Calendar.MINUTE, duration);
        return kalendarz.getTime();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

  

  

    
}